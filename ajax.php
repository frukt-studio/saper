<?php
header("Content-type: text/plain; charset=utf-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
$ajax = $_POST['ajax'];
$post = $_POST;
//$post = removeSlashes($_POST, '",\'');
//$post = removeSlashes($_POST, '",\'');

/*$ajax = 'setBestResults';
$post['name'] = 'Test';
$post['timer'] = 200;
$post['rows'] = 4;
$post['cells'] = 3;
$post['mines'] = 2;*/

//echo "<pre>"; print_r($array); echo "</pre>";
switch($ajax){
	case 'prepareLoadGame':
		$target = $post['target'];
		$dir = 'save';
		$files = scandir($dir);
		foreach($files as $file){ if($file!='.' && $file!='..'){
			//echo $file."<br/>\n";
			$fp = fopen($dir."/".$file, 'r');
			$txt = fgets($fp, 45);
			fclose($fp);
		//	echo "txt=$txt\n";
			$txt = preg_replace('/",.*$/', '', $txt);
		//	echo "txt=$txt\n";
			$txt = preg_replace('/^.*":"/', '', $txt);
			echo '<div style="padding:5px 0 5px 5px;cursor:pointer;width:150px;" onclick="'.$target.'.loadGame(\''.$txt.'\')" ';
			echo 'onmouseover="this.style.backgroundColor=\'#ECECEC\';" onmouseout="this.style.backgroundColor=\'\';" >';
			echo "$txt";
			echo "</div>";
		}}
		break;
	case 'loadGame':
		$file = getFileFromIndex($post['name']);
		$file = file_get_contents($file);
		echo $file;
		break;
	case 'saveGame':
		$post = removeSlashes($post, '",\',\\\\');
		//print_r($post);
		$dataArray = json_decode($post['data'], true);
		print_r($dataArray);
		$json = json_encode($dataArray);
		$json = utf8_urldecode($json, "\\\\");
		echo $json;
		$max = 0;
		$dir = 'save';
		$files = scandir($dir);
		foreach($files as $file){ if($file!='.' && $file!='..'){
			$file = preg_replace("/\.txt$/", '', $file);
			if($max<=$file){ $max = $file+1; }
		}}
		file_put_contents($dir.'/'.$max.".txt", $json);
		break;
	case 'testSaveName':
		$dir = 'save';
		$files = scandir($dir);
		//print_r($files);
		$foo = true;
		foreach($files as $file){ if($file!='.' && $file!='..'){
			//echo $file."<br/>\n";
			$fp = fopen($dir."/".$file, 'r');
			$txt = fgets($fp, 39);
			fclose($fp);
			$prega = "/^$post[name]$/";
		//	echo "prega = $prega\n";
		//	echo "txt=$txt\n";
			$txt = preg_replace('/",.*$/', '', $txt);
		//	echo "txt=$txt\n";
			$txt = preg_replace('/^.*":"/', '', $txt);
		//	echo "txt=$txt\n";
			if(preg_match($prega, $txt)){
				$foo = false;
			}
		}}
		if(!$foo){
			echo '{"error":"name_not_empty","value":"'.($post['name']).'"}';
		}else{
			echo '{"newName":"'.$post['name'].'"}';
		}
		break;
	case 'setBestResults':
		$file = file_get_contents('top10.js');
		$topArray = json_decode($file, true);
		$error = true;
		foreach($topArray as $key=>$value){
			if($value['gameType']['rows'] == $post['rows']
			&& $value['gameType']['cells'] == $post['cells']
			&& $value['gameType']['mines'] == $post['mines']){
				$error = false;
				$array = $value['data'];
				$array[] = array(
					"name"=>$post['name'],
					"time"=>$post['timer'],
					"date"=>date('d.m.Y')
				);
				usort( $array, 
					create_function(   
						'$a,$b', 
						'return ($a["time"] - $b["time"]);' 
					)
				);
				$j=0;
				$newArray = array();
				while($j<10){
					if($array[$j]){
						$newArray[] = $array[$j];
					}
					$j++;
				}
				//usort($array);
				$topArray[$key]['data'] = $newArray;
			}
		}
		if($error){
			$topArray[] = array(
				"gameType"=>array(
					"rows"=>$post['rows'],
					"cells"=>$post['cells'],
					"mines"=>$post['mines']
				),
				"data"=>array("0"=>array(
					"name"=>$post['name'],
					"time"=>$post['timer'],
					"date"=>date('d.m.Y')
				))
			);
		}
		//echo "<pre>"; print_r($topArray); echo "</pre>";
		$json = json_encode($topArray);
		echo $json;
		file_put_contents('top10.js', $json);
		break;
	case 'isBestResult':
		$file = file_get_contents('top10.js');
		$array = json_decode($file, true);
		$error = true;
		foreach($array as $value){
			if($value['gameType']['rows'] == $post['rows']
			&& $value['gameType']['cells'] == $post['cells']
			&& $value['gameType']['mines'] == $post['mines']){
				$error = false;
				$ret = false;
				$j = 0;
				foreach($value['data'] as $res){
					if($post['timer']<$res['time']){
						$ret = true;
					}
					$j++;
				}
				if($ret || $j<10){
					$json = '{"besttime":"add"}';
					echo $json;
					exit;
				}else{
					$json = '{"besttime":"false"}';
					echo $json;
					exit;
				}
			}
		}
		if($error){
			$json = '{"besttime":"add"}';
			echo $json;
		}
		break;
	case 'getBestResults':
		//print_r($post);
		$file = file_get_contents('top10.js');
		$array = json_decode($file, true);
		$error = true;
		foreach($array as $value){
			if($value['gameType']['rows'] == $post['rows']
			&& $value['gameType']['cells'] == $post['cells']
			&& $value['gameType']['mines'] == $post['mines']){
				$value['error'] = 'false';
				$json = json_encode($value['data']);
				echo $json;
				$error = false;
				break;
			}
		}
		if($error){
			echo '{"error":"undefined"}';
		}
		break;
	default:
		echo "{\"ajax\":\"ok\",\"date\":\"".(date('d.m.Y'))."\"}";
		break;
}
//****************************
function prepareHistory($array){
	
}
//****************************
function getFileFromIndex($name){
	$dir = 'save';
	$files = scandir($dir);
	$foo = true;
	foreach($files as $file){ if($file!='.' && $file!='..'){
		$fp = fopen($dir."/".$file, 'r');
		$txt = fgets($fp, 39);
		fclose($fp);
		$prega = "/^$name$/";
		$txt = preg_replace('/",.*$/', '', $txt);
		$txt = preg_replace('/^.*":"/', '', $txt);
		if(preg_match($prega, $txt)){
			return $dir."/".$file;
		}
	}}
}
//****************************
function utf8_urldecode($str, $prefix="%") {
	$str = preg_replace("/".$prefix."u([01-9a-fA-F]{3,4})/i","&#x\\1;",urldecode($str));
	return html_entity_decode($str,null,'UTF-8');
}
//****************************
function removeSlashes($string, $params=false){
	if(!$params){
		$params = '",\'';
	}
	$params = explode(",", $params);
	foreach($params as $param){
		$prega = "/\\\\$param/";
		
		if(is_array($string)){
			foreach($string as $key=>$str){
				if(is_array($str)){
					//echo "ARRAY\n";
					$string[$key] =  removeSlashes($str, $param);
				}else{
					$string[$key] = preg_replace($prega, $param, $str);
				}
			}
		}else{
			$string = preg_replace($prega, $param, $string);
		}
	}
	//print_r($string);
	return $string;
}