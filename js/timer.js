var myTimer = myTimer || {};

myTimer.Timer = function(){
	this.count = 0;
	this.speed = 1000;
	this.extend = function(obj1, obj2){
		for (var prop in obj2){
			obj1[prop] = obj2[prop];
			//console.log(prop);
		}
	};
}

myTimer.Timer.prototype = {
	startTimer:function(params){
		params = params || {};
		this.extend(this, params);
		if(typeof this.timeout =='undefined' || !this.timeout){
			//console.log('startTimer');
			this.timer();
		}
	},
	timer:function(){
		//console.log('timer');
		if(typeof this.timeout !='undefined' && this.timeout){
			clearTimeout(this.timeout);
		}
		_this = this;
		(function(_this){
			_this.timeout = setTimeout(function(){
				_this.timer();
			}, _this.speed);
		})(_this);
		//console.log('count='+this.count);
		this.count++;
		this.onTimer();
		if(this.ontimer && typeof this.ontimer=='function'){
			this.ontimer();
		}
	},
	stopTimer:function(){
		if(typeof this.timeout !='undefined' && this.timeout){
			clearTimeout(this.timeout);
		}
		this.timeout = false;
		this.count = 0;
	},
	onTimer:function(){
		
	},
	setZeros:function(digit){
		//console.log('digit='+digit);
		var minutes = digit/60;
		var minutes = minutes.toString().replace(/\..*$/gi, '');
		var seconds = digit - 60*minutes;
		var ret = minutes+":"+seconds;
		ret = ret.replace(/:[0-9]{1}$/gi, ":0"+seconds);
		ret = ret.replace(/^[0-9]{1}:/gi, "0"+minutes+":");
		return ret;
	}
}