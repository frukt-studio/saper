var ajax = ajax || {}

ajax.Ajax = function(){
	this.ajaxFile = 'ajax.php';
	this.cancelInerval = 10000;
	this.dataType = 'text';
	//****************************************
	this.extend = function(obj1, obj2){
		for (var prop in obj2){
			obj1[prop] = obj2[prop];
		}
	};
};

ajax.Ajax.prototype = {
	loadXMLDoc:function(data){
		if(window.XMLHttpRequest) {
			try {
				var req = new XMLHttpRequest();
			} catch (e){}
		} else if (window.ActiveXObject) {
			try {
				var req = new ActiveXObject('Msxml2.XMLHTTP');
			} catch (e){
				try {
					var req = new ActiveXObject('Microsoft.XMLHTTP');
				} catch (e){}
			}
		}
		
		if(req){
			var _req = req;
			var _this = this;
			this.data = data;
			(function(_this, _req){
				_req.onreadystatechange = function(){
					(function(_this, _req){
						if(_req.readyState == 4){
							clearTimeout(_this.reqTimeout);
							if(_req.status == 200){
								//console.log(_req);
								//console.log(_req.responseText);
								//var response = eval("("+_req.responseText+")") || {}
								//data.onload(response);
								data.onload(_req.responseText);
							}else{
								console.log("�� ������� �������� ������:\n���������� ��������� ������\n" + _req.statusText);
							}
						}
					})(_this, _req);
				}
				_req.open("POST", _this.ajaxFile, true);
				_req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				//console.log(_this.data);
				console.log(_this.convertData(_this.data));
				_req.send(_this.convertData(_this.data));
				_this.reqTimeout = setTimeout(function(){
					(function(_req){
						_req.abort();
					})(_req);
				}, _this.cancelInerval);
			})(_this, _req);
		}else{
			console.log("������� �� ������������ AJAX");
		}
	},
	send:function(params){
		//params = params || {};
		//this.extend(this, params);
		this.loadXMLDoc(params);
	},
	convertData:function(data){
		var ret = "";
		for(var j in data){
			if(j!='onload'){
				if(typeof data[j] == 'object'){
					ret += j + "=" + encodeURIComponent(JSON.stringify(data[j])) + "&";
				}else{
					ret += j + "=" + encodeURIComponent(data[j]) + "&";
				}
			}
		}
		ret = ret.replace(/&$/gi, '');
		return ret;
	},
	Utf8:{
		// public method for url encoding
		encode : function (string) {
			string = string.replace(/\r\n/g,"\n");
			var utftext = "";
			for (var n = 0; n < string.length; n++) {
				var c = string.charCodeAt(n);
				if (c < 128) {
					utftext += String.fromCharCode(c);
				}
				else if((c > 127) && (c < 2048)) {
					utftext += String.fromCharCode((c >> 6) | 192);
					utftext += String.fromCharCode((c & 63) | 128);
				}
				else {
					utftext += String.fromCharCode((c >> 12) | 224);
					utftext += String.fromCharCode(((c >> 6) & 63) | 128);
					utftext += String.fromCharCode((c & 63) | 128);
				}
			}
			return utftext;
		},
		// public method for url decoding
		decode : function (utftext) {
			var string = "";
			var i = 0;
			var c = c1 = c2 = 0;
			while ( i < utftext.length ) {
				c = utftext.charCodeAt(i);
				if (c < 128) {
					string += String.fromCharCode(c);
					i++;
				}
				else if((c > 191) && (c < 224)) {
					c2 = utftext.charCodeAt(i+1);
					string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
					i += 2;
				}
				else {
					c2 = utftext.charCodeAt(i+1);
					c3 = utftext.charCodeAt(i+2);
					string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
					i += 3;
				}
			}
			return string;
		}
	}
}