var mySapper = mySapper || {};

mySapper.Sapper = function(){
	this.timer = new myTimer.Timer; //������������� �������
	this.ajaxSender = new ajax.Ajax;
	this.gameStatuses = ['empty','ready','start','play','win','lose','stop','pause']; //��� ���� (� ��������� �� ������������)
	this.gameStatus = 'empty'; //������� ��������� ����
	this.target = 'sapper'; //����������� �������������� ���� � ������� ����� ��������� ���������
	this.rows = 9; //������
	this.cells = 9; //������
	this.minesCount = 10; //���������� ���
	this.minesFind = 0; //���������� ����� � "����������" ������
	this.winTime = 0;
	this.gameType = 1;
	this.IE = '\v'=='v'; //�������� IE
	this.autoUndoRecord = false; //�������������� ������ � ������� ��� ������������
	this.historyLevel = 0; //������� �������
	this.bombs = {};
	this.autoStep = false;
	//****************************************
	this.extend = function(obj1, obj2){
		for (var prop in obj2){
			if(prop!='timer' && prop!='historyLevel'){
				obj1[prop] = obj2[prop];
			}
		}
	};
}

mySapper.Sapper.prototype = {
	initGame:function(params){
		this.gameStatus = 'empty';
		//console.log(this.rows, this.cells, this.minesCount);
		this.bombs = {};
		this.stopTimer();
		this.winTime = 0;
		this.historyLevel = 0;
		params = params || {};
		this.extend(this, params);
		var obj = document.getElementById(this.target);
		obj.className = 'saper-wrapper';
		obj.innerHTML = getSapperHTML(this.target);
		document.getElementById(this.target+'_rows').value = this.rows;
		document.getElementById(this.target+'_cells').value = this.cells;
		document.getElementById(this.target+'_mines').value = this.minesCount;
		document.getElementById(this.target+'_gameSelector').value = this.gameType;
		if(this.gameType==1 || this.gameType==2 || this.gameType==3){
			document.getElementById(this.target + '_matrix').style.display = 'none';
			document.getElementById(this.target + '_gameSelector').value = this.gameType;
		}else if(this.gameType==4){
			document.getElementById(this.target + '_matrix').style.display = '';
			document.getElementById(this.target + '_gameSelector').value = this.gameType;
		}
		this.table = document.getElementById(this.target).getElementsByTagName('table')[0];
		//console.log(this.table);
		for(var rj=0; rj<this.rows; rj++){
			var row = this.table.insertRow(rj);
			for(var cj=0; cj<this.cells; cj++){
				var cell = row.insertCell(cj);
				cell.className = 'icon'; // showbomb';
				cell.hasBomb = false;
				cell.opening = false;
				cell.oldClass = {};
				if(this.IE){ cell.innerHTML = '&nbsp;'; }
			}
		}
		this.minesFind = 0;
		this.initMines(true);
		//this.showBombs();
		this.initOnclick();
		document.getElementById(this.target+'_mineCount').innerHTML = this.minesCount;
		this.gameStatus = 'ready';
	},
	initGameSettings:function(id){
		this.rows = document.getElementById(this.target+'_rows').value;
		this.cells = document.getElementById(this.target+'_cells').value;
		this.minesCount = document.getElementById(this.target+'_mines').value;
		this.initGame();
	},
	initGameFromGameType:function(type){
		if(type==4){
			document.getElementById(this.target + '_matrix').style.display = '';
			this.gameType = 4;
		}else{
			document.getElementById(this.target + '_matrix').style.display = 'none';
			if(type==1){
				this.rows = 9;
				this.cells = 9;
				this.minesCount = 10;
				this.gameType = 1;
				this.initGame();
			}else if(type==2){
				this.rows = 16;
				this.cells = 16;
				this.minesCount = 40;
				this.gameType = 2;
				this.initGame();
			}else if(type==3){
				this.rows = 16;
				this.cells = 30;
				this.minesCount = 99;
				this.gameType = 3;
				this.initGame();
			}
		}
	},
	initMines: function(cicle){
		//console.log('initMines');
		//console.log(this.rows, this.cells, this.minesCount);
		if(cicle){
			for(var j=0; j<this.minesCount; j++){
				rIndex = Math.round(Math.random() * (this.rows-1));
				cIndex = Math.round(Math.random() * (this.cells-1));
				//console.log("rIndex="+rIndex+" :: cIndex="+cIndex);
				if(this.table.rows[rIndex].cells[cIndex].hasBomb){
					var indexes = this.initMines();
					this.table.rows[indexes.rIndex].cells[indexes.cIndex].hasBomb = true;
					this.bombs[j] = {"y":indexes.rIndex,"x":indexes.cIndex};
				}else{
					this.table.rows[rIndex].cells[cIndex].hasBomb = true;
					this.bombs[j] = {"y":rIndex,"x":cIndex};
				}
			}
		}else{
			rIndex = Math.round(Math.random() * (this.rows-1));
			cIndex = Math.round(Math.random() * (this.cells-1));
			if(this.table.rows[rIndex].cells[cIndex].hasBomb){
				return this.initMines();
			}else{
				return {rIndex:rIndex,cIndex:cIndex};
			}
		}
		//this.tempo();
	},
	showBombs: function(){
		//var objs = this.table.getElementsByClassName('flag');
		var objs = this.getElementsBy('.flag', this.table);
		console.log(objs);
		for(var j=0; j<objs.length; j++){
			var obj = objs[j];
			if(obj.hasBomb!=1){
				obj.className = obj.className.replace(/ ?flag/gi, '');
				obj.className = obj.className.replace(/ ?quest/gi, '');
				obj.className += ' wrong';
			}
		}
		
		for(var rj=0; rj<this.table.rows.length; rj++){
			for(var cj=0; cj<this.table.rows[rj].cells.length; cj++){
				var cell = this.table.rows[rj].cells[cj];
				if(cell.hasBomb){
					cell.className += ' showbomb';
				}
			}
		}
	},
	initOnclick: function(e){
		//console.log('initOnclick');
		for(var rj=0; rj<this.table.rows.length; rj++){
			for(var cj=0; cj<this.table.rows[rj].cells.length; cj++){
				var cell = this.table.rows[rj].cells[cj];
				var _this = this;
					//**********************
							(function(_this, cell, y, x){
								cell.onmouseup = function(e){
									if(!cell.opening){
										//console.log('mouse up and autoUndoRecord=true');
										_this.autoUndoRecord = {x:x,y:y};
										//console.log('start auto undo');
										//console.log('_this.autoUndoRecord.x='+_this.autoUndoRecord.x);
										//console.log('_this.autoUndoRecord.y='+_this.autoUndoRecord.y);
										this.historyLevel = _this.historyLevel;
										//console.log('_this.historyLevel='+_this.historyLevel);
										if(cell.style.outline!='none'){
											cell.style.outline='none';
										}
									}
									cell.clicked = {x:x,y:y};
								}
							})(_this, cell, rj, cj);
					//**********************
							(function(_this, cell, y, x){
								cell.onclick = function(e){
									if(!cell.opening){
										//console.log(cell);
										if(_this.gameStatus != 'play'){
											_this.gameStatus = 'start';
											_this.startTimer();
										}
										var foo = _this.initDigits(y,x);
										if(foo=='bomb'){
											console.log('BOMB !!!');
											if(cell.hasBomb){
												var params =  {
													'title':'���������!',
													'content':'������ ����� �����!!!<br/>����������?',
													onmokbtn:function(){
														(function(cell, _this){
															//_this.setBestResults();
															cell.className += ' showbombexplode';
															cell.opening = true;
															cell.historyLevel = this.historyLevel;
															cell.startGame = false;
															//**********
															_this.gameStatus = 'lose';
															_this.stopGame();
														})(cell, _this);
													},
													oncloseafter:function(){
														(function(cell, _this){
															if(cell.opening != true){
																//_this.autoCancel = true;
																_this.undoStep();
																//_this.autoUndo = false;
																cell['clicked'] = undefined;
																try{
																	delete cell['clicked'];
																}catch(e){}
															}
														})(cell, _this);
													}
												}
												myModal.showModal(params);
											}
											
										}else if(foo=='empty'){
											_this.openEmpty(x,y)
										}
										_this.gameStatus = 'play';
										//this.parentNode.parentNode.parentNode.startGame = true;
										//startGame(this.parentNode.parentNode.parentNode.id);
										if(this.gameStatus!='win'){
											_this.testToWin();
										}
										//console.log('_this.autoUndoRecord.x='+_this.autoUndoRecord.x);
										//console.log('_this.autoUndoRecord.y='+_this.autoUndoRecord.y);
										if(typeof _this.autoUndoRecord == 'object' && x==_this.autoUndoRecord.x && y==_this.autoUndoRecord.y){
											_this.autoUndoRecord = false;
						//					console.log('stop auto undo');
											_this.autoUndoRecord = false;
											_this.historyLevel++;
											if(_this.autoUndo){
										//		_this.undoStep();
										//		_this.autoUndo = false;
										//		cell['clicked'] = undefined;
										//		try{
										//			delete cell['clicked'];
										//		}catch(e){}
											}
										}
										if(_this.autoStep){
											_this.questStep();
										}
									}
								}
							})(_this, cell, rj, cj);
					//**********************
							(function(_this, cell, y, x){
								cell.oncontextmenu = function (){
									
											//console.log(this.opening, this.parentNode.parentNode.parentNode.stopGame);
											if(!this.opening && !this.parentNode.parentNode.parentNode.stopGame){
												//console.log('inside', _this.minesFind, _this.minesCount);
												if(this.className.match(/ ?flag/gi)){
													this.className = this.className.replace(/ ?flag/gi, '');
													this.className = this.className.replace(/ ?quest/gi, '');
													this.className += ' quest';
													this.oldClass[_this.historyLevel] = 'icon flag';
													_this.minesFind--;
													_this.setMinesCount(_this.minesFind);
												}else if(this.className.match(/ ?quest/gi)){
													this.className = this.className.replace(/ ?flag/gi, '');
													this.className = this.className.replace(/ ?quest/gi, '');
													this.oldClass[_this.historyLevel] = 'icon quest';
												}else if(_this.minesFind+1 <= _this.minesCount){
													this.className = this.className.replace(/ ?flag/gi, '');
													this.className = this.className.replace(/ ?quest/gi, '');
													this.className += ' flag';
													//console.log('flag');
													_this.minesFind++;
													_this.setMinesCount(_this.minesFind);
													this.oldClass[_this.historyLevel] = 'icon';
												}
											}
											if(this.gameStatus!='win'){
												_this.testToWin();
											}
											if(typeof _this.autoUndoRecord == 'object' && x==_this.autoUndoRecord.x && y==_this.autoUndoRecord.y){
												_this.autoUndoRecord = false;
							//					console.log('stop auto undo');
												_this.autoUndoRecord = false;
												_this.historyLevel++;
												//if(_this.autoUndo){
												//	_this.undoStep();
												//	_this.autoUndo = false;
												//	cell['clicked'] = undefined;
												//	try{
												//		delete cell['clicked'];
												//	}catch(e){}
												//}
											}
											if(_this.autoStep){
												_this.questStep();
											}
											return false;
										}
							})(_this, cell, rj, cj);
			}
		}
	},
	initDigits: function(rj,cj){
	//	console.log('initDigits');
	//	console.log(rj,cj);
	//	for(var rj=0; rj<table.rows.length; rj++){
	//		for(var cj=0; cj<table.rows[rj].cells.length; cj++){
				var numCell = this.table.rows[rj].cells[cj];
				//numCell.onclick = false;
				//numCell.opening = true;
				if(!numCell.hasBomb){
					if(numCell.style.outline!='none'){
						numCell.style.outline='none';
					}
					if(numCell.className=='icon'){
						//var bombCell = table.rows[rj].cells[cj];
						var startRow = ((rj==0)?0:rj-1);
						var endRow = ((rj==this.rows-1)?this.rows-1:rj+1);
						var startCell = ((cj==0)?cj:cj-1);
						var endCell = ((cj==this.cells-1)?this.cells-1:cj+1);
						var mlog = 'rj='+rj+'  ::  cj='+cj;
						mlog += '\n----------------\n';
						mlog += 'startRow='+startRow+'\n'
						mlog += 'endRow='+endRow+'\n';
						mlog += 'startCell='+startCell+'\n';
						mlog += 'endCell='+endCell+'\n';
						mlog += '\n----------------';
						//console.log(mlog);
						var count = 0;
						for(var rjj=startRow; rjj<endRow+1; rjj++){
							for(var cjj=startCell; cjj<endCell+1; cjj++){
								var cell = this.table.rows[rjj].cells[cjj];
								if(cell.hasBomb){
									count++;
								}
							}
						}
						if(count>0){
							if(this.autoUndoRecord && (cj!=this.autoUndoRecord.x || rj!=this.autoUndoRecord.y) && typeof numCell.historyLevel=='undefined'){
								numCell.historyLevel = this.historyLevel;
							}
							numCell.className = numCell.className.replace(/ ?digit/gi, '');
							numCell.className += ' digit';
							numCell.innerHTML = count;
							numCell.opening = true;
							if(numCell.historyLevel=='undefined'){
								numCell.historyLevel = this.historyLevel;
							}
							if(numCell.className.match(/ ?flag/gi)){
								this.minesFind--;
								this.setMinesCount(this.minesFind);
							}
							numCell.className = numCell.className.replace(/ ?flag/gi, '');
							numCell.className = numCell.className.replace(/ ?quest/gi, '');
							return 'digit';
						}else{
							numCell.className = numCell.className.replace(/ ?empty/gi, '');
							numCell.className += ' empty';
							return 'empty';
						}
					}
				}else{
					numCell.className = numCell.className.replace(/ ?flag/gi, '');
					numCell.className = numCell.className.replace(/ ?quest/gi, '');
					numCell.className = numCell.className.replace(/ ?showbombexplode/gi, '');
					
					//this.explodeBomb();
					//stopGame(numCell.parentNode.parentNode.parentNode);
					//numCell.onclick = false;
					return 'bomb';
				}
	//		}
	//	}
	},
	openEmpty: function(x,y){
		//console.log('openEmpty');
		var myCell = this.table.rows[y].cells[x];
		if(!myCell.opening){
			if(myCell.style.outline!='none'){
				myCell.style.outline='none';
			}
			if(this.autoUndoRecord && (x!=this.autoUndoRecord.x || y!=this.autoUndoRecord.y) && typeof myCell.historyLevel=='undefined'){
				myCell.historyLevel = this.historyLevel;
			}
			myCell.opening = true;
			myCell.historyLevel = this.historyLevel;
			if(myCell.className.match(/ ?flag/gi)){
				this.minesFind--;
				this.setMinesCount(this.minesFind);
			}
			myCell.className = myCell.className.replace(/ ?flag/gi, '');
			myCell.className = myCell.className.replace(/ ?quest/gi, '');
			var startRow = ((y==0)?0:y-1);
			var endRow = ((y==this.rows-1)?this.rows-1:y+1);
			var startCell = ((x==0)?x:x-1);
			var endCell = ((x==this.cells-1)?this.cells-1:x+1);
			var mlog = 'x='+x+'  ::  y='+y;
			mlog += '\n----------------\n';
			mlog += 'startRow='+startRow+'\n'
			mlog += 'endRow='+endRow+'\n';
			mlog += 'startCell='+startCell+'\n';
			mlog += 'endCell='+endCell+'\n';
			mlog += '\n----------------';
			//console.log(mlog);
			for(var rjj=startRow; rjj<endRow+1; rjj++){
				for(var cjj=startCell; cjj<endCell+1; cjj++){
					var foo = this.initDigits(rjj, cjj);
					//console.log(foo);
					var cell = this.table.rows[rjj].cells[cjj];
					if(foo=='empty'  && !cell.opening){
						//console.log(cell.className);
						//console.log('most openEmpty', cjj, rjj);
						this.openEmpty(cjj, rjj);
					}
				}
			}
		}
	},
	setMinesCount:function(){
		document.getElementById(this.target+'_mineCount').innerHTML = this.minesCount-this.minesFind;
	},
	testToWin:function(){
		//console.log('testToWin');
		//var objs = this.table.getElementsByClassName('flag');
		//var objs = this.table.querySelector('.flag');
		var lens = 0;
		var objs = this.table.getElementsByTagName('td');
		for(var j=0; j<objs.length; j++){
			var td = objs[j];
			if(td.className=='icon'){
				lens++;
			}
		}
		var objs = this.getElementsBy('.flag', this.table);
		//console.log(objs);
		var win = true;
		for(var j=0; j<objs.length; j++){
			var td = objs[j];
			if(!td.hasBomb){
				win = false;
			}
		}
		//console.log('win='+win+' :: '+'objs.length='+objs.length+' :: '+'minesCount='+this.minesCount+' :: '+'minesFind='+this.minesFind+' :: '+'lens='+lens);
		if(win && objs.length>0 && this.minesCount == this.minesFind && lens<=1){
			this.gameStatus = 'win';
			this.winTime = this.getTimerSeconds();
			this.openAll();
			this.stopGame();
			this.gameStatus=='win';
		}else{
			//console.log('no Win');
		}
		return win;
	},
	openAll:function(){
		var objs = this.table.getElementsByTagName('td');
		for(var j=0; j<objs.length; j++){
			var td = objs[j];
			if(td.className=='icon'){
				td.onclick();
			}
		}
	},
	stopGame:function(){
		var _this = this;
		var objs = this.table.getElementsByTagName('td');
		for(var j=0; j<objs.length; j++){
			var td = objs[j];
			td.opening = true;
			td.historyLevel = this.historyLevel;
			//console.log(td);
		}
		this.stopTimer();
		this.showBombs();
		//console.log(this.gameStatus);
		if(this.gameStatus=='win'){
			//console.log('WIN');
			//alert('������');
			var params =  {
				'title':'����������!',
				'content':'�����������, �� ��������',
				oncloseafter:function(){
					_this.isBestResult();
				}
			}
			myModal.showModal(params);
			this.gameStatus = 'stop';
			
			
		}else if(this.gameStatus=='lose'){
			//setTimeout("confirm('��������� ��������  � sms-���������?')", 50);
		}
		this.autoStep = false;
	},
	getCellPosition:function(obj){
		var x,y = 0;
		for(var j=0; j<this.table.rows.length; j++){
			for(var jj=0; jj<this.table.rows[j].length; jj++){
				if(obj = this.table.rows[j].cells[jj]){
					return {y:j,x:jj};
				}
			}
		}
	},
	//****************************************
	undoStep:function(){
		if(this.historyLevel>0){
			var level = this.historyLevel - 1;
			this.historyLevel = level;
			console.log('level='+level);
			for(var y=0; y<this.table.rows.length; y++){
				for(var x=0; x<this.table.rows[y].cells.length; x++){
					var obj = this.table.rows[y].cells[x];
					if(obj.historyLevel == level){
						obj.className = 'icon';
						obj.opening = false;
						obj.innerHTML = '&nbsp';
						if(typeof obj.oldClass[level]!='undefined'){
							obj.className = obj.oldClass[level];
							if(obj.className == 'icon'){
								this.minesFind--;
								this.setMinesCount(this.minesFind);
							}
							console.log("level="+level+" :: oldClass="+obj.oldClass[level]);
							obj.oldClass[level] = undefined;
							try{
								delete obj.oldClass[level];
							}catch(e){}
						}
						var j = false;
						for(var j in obj.oldClass){
							foo = j;
						}
						if(j){
							console.log(obj.oldClass);
							obj.historyLevel = j;
						}else{
							obj['historyLevel'] = undefined;
							try{
								delete obj['historyLevel'];
							}catch(e){}
						}
					}
				}
			}
		}
	},
	//****************************************
	setBestResults:function(){
		console.log('setBestResults');
		var _this = this;
		if(document.getElementById(this.target+'_username')){
			var value = document.getElementById(this.target+'_username').value;
			if(value==''){
				setTimeout(function(){
					_this.confirmBestResults(' <font color=red>��� �� ����� ���� ������</font>');
				}, 20);
			}else if(value.length>20){
				setTimeout(function(){
					_this.confirmBestResults(' <font color=red>��� �� ������ ���� ������� 20 ��������</font>', value);
				}, 20);
			}else if(!value.match(/[A-Za-z�-��-�]{1}[A-Za-z�-��-�0-9 ]*$/)){
				setTimeout(function(){
					_this.confirmBestResults(' <font color=red>��� ������ ���������� � �����<br/>� ��������� �����, ������ ��� �����</font>', value);
				}, 20);
			}else{
				this.ajax({
					ajax:'setBestResults',
					rows:this.rows,
					cells:this.cells,
					mines:this.minesCount,
					timer:this.winTime,
					name:value,
					onload:function(result){
						console.log(result);
						_this.getBestResults();
					}
				});
			}
		}else{
			setTimeout(function(){
				_this.confirmBestResults(true);
			}, 20);
		}
	},
	confirmBestResults:function(error, name){
		console.log('confirmBestResults');
		var _this = this;
		var inner = '<div>';
		inner += '�� ����� � ���-10 ��������, ��������?<br/><br/>';
		inner += '���� ���:';
		if(error){
			inner += error;
		}
		inner += '<br/>';
		inner += '<input id="'+this.target+'_username" type="text" style="height:25px;" ';
		if(name){
			inner += ' value="'+name+'" ';
		}
		inner += '/></div>';
		var params =  {
			'title':'�����������!',
			'content':inner,
			onmokbtn:function(){
				(function(_this){
					_this.setBestResults();
				})(_this);
			}
		}
		myModal.showModal(params);
	},
	isBestResult:function(){
		console.log('isBestResult');
		var _this = this;
		this.ajax({
			ajax:'isBestResult',
			rows:this.rows,
			cells:this.cells,
			mines:this.minesCount,
			timer:this.winTime,
			onload:function(result){
				console.log(result);
				var res = eval("("+result+")");
				if(res.error=='undefined'){
					result = '�� ������� �������� �� ��������� ���������� ����';
				}else{
					if(res.besttime=='add'){
						_this.confirmBestResults();
					}
				}
			}
		});
	},
	getBestResults:function(){
		var _this = this;
		this.ajax({
			ajax:'getBestResults',
			rows:this.rows,
			cells:this.cells,
			mines:this.minesCount,
			onload:function(result){
				//console.log(result);
				var res = eval("("+result+")");
				//console.log(res);
				if(res.error=='undefined'){
					result = '�� ������� �������� �� ��������� ���������� ����';
				}else{
					var inner = '<table cellpadding="0" cellspacing="0" border="0" width="100%" style="font-size:12px;">';
					inner += '<tr><th width="20">&nbsp;</th>';
					inner += '<th align="left" height="25">���</th>';
					inner += '<th width="100" align="left">�����</th>';
					inner += '<th width="100" align="left">����</th>';
					inner += '</tr>';
					for(var j in res){
						inner += '<tr><td width="20">'+(j*1+1)+'</td>';
						inner += '<td height="25">'+res[j]['name']+'</td>';
						inner += '<td width="100">'+_this.convertTimer(res[j]['time'])+'</td>';
						inner += '<td width="100">'+res[j]['date']+'</td>';
						inner += '</tr>';
					}
					result = inner;
				}
				var params =  {
					'title':'���-10 ����������� ����',
					'content':result
				}
				myModal.showModal(params);
			}
		});
	},
	//****************************************
	startTimer:function(){
		_this = this;
		this.timer.startTimer({
			parentClass:_this,
			ontimer:function(){
				document.getElementById(this.parentClass.target+'_myTimer').innerHTML = this.parentClass.getTimer();
			}
		});
	},
	getTimerSeconds:function(){
		return this.timer.count-1;
	},
	getTimer:function(){
		return this.timer.setZeros(this.timer.count-1);
	},
	convertTimer:function(t){
		return this.timer.setZeros(t);
	},
	setTimer:function(t){
		this.timer.count = t;
	},
	stopTimer:function(){
		this.timer.stopTimer();
	},
	//****************************************
	prepareSaveGame:function(error){
		var _this = this;
		var inner = '������� ��������: ';
		if(error){
			inner += error;
		}
		inner += '<br/><input type="text" maxlength="15" id="'+this.target+'_saveGame" ';
		inner += 'placeholder="�������� ����" style="height:25px;margin-top:5px;" value="����-4" />';
		var params =  {
			'title':'���������� ����',
			'content':inner,
			onmokbtn:function(){
				(function(_this){
					_this.testSaveName(document.getElementById(_this.target+'_saveGame').value);
				})(_this);
			}
		}
		myModal.showModal(params);
	},
	testSaveName:function(name){
		var _this = this;
		this.ajax({
			ajax:'testSaveName',
			name:name,
			target:_this.target,
			onload:function(result){
				console.log(result);
				var data = eval("("+result+")");
				if(data.error){
					//console.log(data.error);
					setTimeout(function(){
						_this.prepareSaveGame(' <font color=red>����� �������� ��� ����</font>', data.value);
					}, 20);
				}else if(data.newName){
					_this.saveGame(data.newName);
				}
			}
		});
	},
	saveGame:function(name){
		console.log('saveGame');
		//console.log(JSON.stringify(this.bombs));
		//**********************
		var save = {};
		save.name = name,
		save.data = {
			rows:this.rows,
			cells:this.cells,
			minesCount:this.minesCount,
			timer:this.getTimerSeconds(),
			historyLevel:this.historyLevel
			//target:this.target
		};
		save.bombs = this.bombs;
		save.history = {};
		for(var level=0; level<this.historyLevel; level++){
			//if(this.historyLevel>0){
				//console.log('level='+level);
				for(var y=0; y<this.table.rows.length; y++){
					for(var x=0; x<this.table.rows[y].cells.length; x++){
						var obj = this.table.rows[y].cells[x];
						if(obj.historyLevel == level && obj.clicked){
							//console.log(obj.className);
							save.history[level] = obj.clicked;
							save.history[level]['class'] = obj.oldClass;
						}
					}
				}
			//}
		}
		console.log(JSON.stringify(save));
		var saver = JSON.stringify(save);
		//**********************
		var params = {
			ajax:'saveGame',
			data:saver,
			onload:function(result){
				console.log(result);
			}
		}
		this.ajax(params);
	},
	//****************************************
	loadGame:function(gameName){
		console.log('loadGame');
		console.log('this.target', this.target);
		var _this = this;
		var params = {
			ajax:'loadGame',
			name:gameName,
			onload:function(result){
				//console.log(result);
				(function(_this, result){
					var data = eval("("+result+")");
					_this.initLoadGame(data);
				})(_this, result);
			}
		}
		this.ajax(params);
	},
	initLoadGame:function(params){
		this.stopGame();
		this.bombs = {};
		this.stopTimer();
		this.minesFind = 0;
		this.historyLevel = 0;
		//console.log(params);
		myModal.closeModal(myModal.getTopId());
		var data = params.data;
		//****************
		this.gameStatus = 'empty';
		params = params || {};
		this.extend(this, data);
		//****************
		console.log('this.target', this.target);
		var obj = document.getElementById(this.target);
		obj.className = 'saper-wrapper';
		obj.innerHTML = getSapperHTML(this.target);
		document.getElementById(this.target+'_rows').value = this.rows;
		document.getElementById(this.target+'_cells').value = this.cells;
		document.getElementById(this.target+'_mines').value = this.minesCount;
		document.getElementById(this.target+'_gameSelector').value = this.gameType;
		if(this.gameType==1 || this.gameType==2 || this.gameType==3){
			document.getElementById(this.target + '_matrix').style.display = 'none';
			document.getElementById(this.target + '_gameSelector').value = this.gameType;
		}else if(this.gameType==4){
			document.getElementById(this.target + '_matrix').style.display = '';
			document.getElementById(this.target + '_gameSelector').value = this.gameType;
		}
		this.table = document.getElementById(this.target).getElementsByTagName('table')[0];
		//console.log(this.table);
		for(var rj=0; rj<this.rows; rj++){
			var row = this.table.insertRow(rj);
			for(var cj=0; cj<this.cells; cj++){
				var cell = row.insertCell(cj);
				cell.className = 'icon'; // showbomb';
				cell.hasBomb = false;
				cell.opening = false;
				cell.oldClass = {};
				if(this.IE){ cell.innerHTML = '&nbsp;'; }
			}
		}
		
		this.bombs = params.bombs;
		for(var j in this.bombs){
			this.table.rows[this.bombs[j].y].cells[this.bombs[j].x].hasBomb = true;
		}
		//this.showBombs();
		
		this.setTimer(params.data.timer);
		this.initOnclick();
		this.prepareHistory(params);
		
		document.getElementById(this.target+'_mineCount').innerHTML = this.minesCount;
		this.gameStatus = 'play';
		
		//console.log(this);
		this.setMinesCount(this.minesFind);
		console.log('END INIT');
		
	},
	prepareLoadGame:function(){
		var _this = this;
		console.log(this.target);
		var params = {
			ajax:'prepareLoadGame',
			target:this.target,
			onload:function(result){
				//console.log(result);
				(function(_this, result){
					//console.log(result);
					var params =  {
						title:'�������� ����',
						showOkBtn:false,
						content:result
					}
					myModal.showModal(params);
				})(_this, result);
			}
		}
		this.ajax(params);
		
	},
	//****************************************
	prepareHistory:function(params){
		var data = params.data;
		var history = params.history;
		console.log('prepareHistory');
		//console.log(data);
		for(var j=0; j<data.historyLevel; j++){
			//console.log(history[j]);
			if(!history[j]){
				for(var jj in history){
					//console.log(history[jj]);
					if(history[jj]['class'][j]){
						//console.log(history[jj]);
						history[j] = history[jj];
					}
				}
			}
		}
		//console.log(history);
		//console.log(this.getLength(history));
		for(var j=0; j<data.historyLevel; j++){
		//for(var j=0; j<1; j++){
			if(this.getLength(history[j]['class'])==0){
				//console.log('left click');
				this.table.rows[history[j].y].cells[history[j].x].onmouseup();
				this.table.rows[history[j].y].cells[history[j].x].onclick();
			}else{
				//console.log('right click');
				this.table.rows[history[j].y].cells[history[j].x].onmouseup();
				this.table.rows[history[j].y].cells[history[j].x].oncontextmenu();
			}
			//this.table.rows[this.bombs[j].y].cells[this.bombs[j].x].hasBomb = true;
		}
	},
	//****************************************
	getElementsBy:function(value, obj){
		if(value.match(/^\./)){
			value = value.replace(/^\./gi, '')
			//if(this.IE){
				var e = obj.getElementsByTagName('TD');
				var ret = {};
				var count = 0;
				for(var j=0; j<e.length; j++){
					var td = e[j];
					if(td.className == 'icon '+value){
						//console.log(td.className);
						ret[count] = td;
						count++;
					}
				}
				ret.length = count;
				return ret;
			//}else{
			//	return obj.getElementsByClassName('icon '+value);
			//}
		}
	},
	getLength:function(obj){
		var count=0;
		for(var j in obj) count++;
		return count;
	},
	//****************************************
	ajax:function(params){
		var _this = this;
		this.ajaxSender.send(params);
	},
	//****************************************
	//****************************************
	
	//****************************************
	//****************************************
	findQuestStep:function(obj, y, x){
	//	console.log('findQuestStep');
		var startRow = ((y==0)?0:y-1);
		var endRow = ((y==this.rows-1)?this.rows-1:y+1);
		var startCell = ((x==0)?x:x-1);
		var endCell = ((x==this.cells-1)?this.cells-1:x+1);
		var mlog = 'x='+x+'  ::  y='+y;
	//	mlog += '\n----------------\n';
	//	mlog += 'startRow='+startRow+'\n'
	//	mlog += 'endRow='+endRow+'\n';
	//	mlog += 'startCell='+startCell+'\n';
	//	mlog += 'endCell='+endCell+'\n';
	//	mlog += '\n----------------';
	//	console.log(mlog);
		var cellsCount = -1;
		var closeCount = 0;
		var flagCount = 0;
		var digit = false;
		var center = false;
		for(var rjj=startRow; rjj<endRow+1; rjj++){
			for(var cjj=startCell; cjj<endCell+1; cjj++){
				var cell = this.table.rows[rjj].cells[cjj];
				if(rjj==y && cjj==x){
					digit = cell.innerHTML-1+1;
					center = cell;
					center.x = x;
					center.y = y;
				}
				if(cell.className=='icon'){
					closeCount++;
				}
				if(cell.className=='icon flag'){
					//closeCount--;
					flagCount++;
				}
				cellsCount++;
			}
		}
	//	console.log('cellsCount='+cellsCount);
	//	console.log('closeCount='+closeCount);
	//	console.log('flagCount='+flagCount);
	//	console.log('  ('+digit+'-'+flagCount+')=='+closeCount+'  )');
	//	console.log(digit);
		if(  (digit-flagCount)==closeCount  ||  flagCount==digit  ){
			if((digit-flagCount)==closeCount){
				color = '#FF0000';
			}
			if(flagCount==digit){
				color = '#00CC00';
			}
			for(var rjj=startRow; rjj<endRow+1; rjj++){
				for(var cjj=startCell; cjj<endCell+1; cjj++){
					var cell = this.table.rows[rjj].cells[cjj];
					if(cell.className=='icon'){
						//console.log('x='+cjj+' :: y='+rjj);
						this.colorizeCell(cell, center.y, center.x, color);
						//return false;
					}
				}
			}
		}
	},
	colorizeCell:function(obj, y, x, color){
		this.colorIntervalCount=0;
		obj.style.outline = 'solid 1px '+color;
	//	console.log('color '+x+':'+y);
	//	console.log(this.table.rows[y].cells[x]);
	},
	questStep:function(){
		if(this.gameStatus=='play'){
			//console.log('questStep');
			for(var y=0; y<this.table.rows.length; y++){
				for(var x=0; x<this.table.rows[y].cells.length; x++){
					//console.log(y,x);
					var obj = this.table.rows[y].cells[x];
					if(obj.opening==true && obj.className=='icon digit'){
						this.findQuestStep(obj, y, x);
					}
				}
			}
		}
	},
	toggleAutoStep:function(){
		if(this.gameStatus=='play'){
			if(this.autoStep){
				var img = document.getElementById(this.target+'_autostep').getAttribute('src');
				document.getElementById(this.target+'_autostep').setAttribute('src', img.replace(/\_active.gif$/gi, '.gif'));
				this.autoStep = false;
			}else{
				var img = document.getElementById(this.target+'_autostep').getAttribute('src');
				document.getElementById(this.target+'_autostep').setAttribute('src', img.replace(/\.gif$/gi, '_active.gif'));
				this.autoStep = true;
				this.questStep();
			}
			console.log('toggleAutoStep');
		}
	}
}