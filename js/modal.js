var myModal = (function(){
	var id = id || 0;
	var zIndex = zIndex || 1000;
	var IE = '\v'=='v';
	var modalBgCSS = {
		position:this.IE?'fixed':'fixed',
		width:this.IE?'100%':'auto',
		left:'0px',
		right:'0px',
		top:'0px',
		bottom:'0px',
		opacity:'.8',
		backgroundColor:'#666666',
		cursor:'pointer',
		zIndex:zIndex,
		height:IE?'100%':'auto',
		width:IE?'100%':'auto'
	};
	var modalCSS = {
		position:IE?'absolute':'fixed',
		left:'50%',
		marginLeft:'-200px',
		top:'20%',
		minWidth:'400px',
		width:IE?'400px':'auto',
		height:'auto',
		minHeight:'100px',
		backgroundColor:'#FFFFFF',
		border:'solid 4px #EFEFEF',
		padding:IE?'15px 0 15px 15px':'15px',
		zIndex:zIndex+1
	};
	var modalCloserCSS = {
		float:'right',
		width:'20px',
		height:'20px',
		backgroundColor:'#88A6FF',
		color:'#56070C',
		marginLeft:IE?'380px':'0',
		marginTop:'-15px',
		marginRight:IE?'0':'-15px',
		textAlign:'center',
		fontWeight:'bold',
		fontFamily:'Verdana',
		fontSize:'14px',
		cursor:'pointer'
	};
	var modalTitleCSS = {
		float:'left',
		margin:'-15px 0 0 -5px',
		padding:'5px 0 15px 10px',
		/*backgroundColor:'#88A6FF',*/
		fontWeight:'bold',
		fontFamily:'Verdana',
		fontSize:'14px',
		borderBottom:'solid 1px #CCCCCC',
		width:'350px'
	};
	var modalContentCSS = {
		float:'none',
		clear:'both',
		padding:'15px 0 20px 0',
		fontFamily:'Verdana',
		fontSize:'12px'
	};
	var modalOkBtnCSS = {
		marginLeft:'160px',
		padding:'5px 15px 5px 15px'
	}
	var modalElements = ['modalBg_','modal_','modalTitle_','modalOK_'];
	this.onclosebefore = false;
	this.oncloseafter = false;
	this.onmokbtn = false;
	//****************************************
	return{
		showOkBtn:true,
		extend:function(obj1, obj2){
			for (var prop in obj2){
				//console.log(prop, obj2[prop]);
				obj1[prop] = obj2[prop];
			}
		},
		showModal:function(params){
			//console.log(params);
			//console.log(modalCloserCSS);
			params = params || {};
			this.extend(this, params);
			this.createBackground();
			this.createModal();
			zIndex+2;
			id++;
		},
		createBackground:function(){
			//console.log('showBackground id='+id);
			var elem = document.createElement('div');
			elem.id = "modalBg_"+id;
			//if(!IE){}
			document.body.appendChild(elem);
			this.setCSS(elem, modalBgCSS);
			elem.className = 'modalBgCSS';
			_this = this;
			(function(_this, id){
				elem.onclick = 	function(e){
					//console.log(this);
					_this.closeModal(id);
				}
			})(_this, id);
			if(IE){
				//console.log(document.body.scrollHeight);
				if(screen.height > document.body.scrollHeight){
					elem.style['height'] = screen.height + 'px';
				}else{
					elem.style['height'] = document.body.scrollHeight + 'px';
				}
			}
		},
		createModal:function(){
			var elem = document.createElement('div');
			elem.id = "modal_"+id;
			this.setCSS(elem, modalCSS);
			document.body.appendChild(elem);
			var closer = document.createElement('div');
			this.setCSS(closer, modalCloserCSS);
			elem.appendChild(closer);
			closer.innerHTML = 'x';
			_this = this;
			(function(_this, id){
				closer.onclick = 	function(e){
					//console.log(this);
					_this.closeModal(id);
				}
			})(_this, id);
			//*************
			var titleElem = document.createElement('div');
			elem.appendChild(titleElem);
			titleElem.id = 'modalTitle_'+id;
			this.setCSS(titleElem, modalTitleCSS);
			if(this.title){
				titleElem.innerHTML = this.title;
			}else{
				titleElem.style.display = 'none';
			}
			//*************
			var cont = document.createElement('div');
			this.setCSS(cont, modalContentCSS);
			elem.appendChild(cont);
			cont.innerHTML = this.content;
			//*************
			if(this.showOkBtn){
				var mOkBtn = document.createElement('button');
				this.setCSS(mOkBtn, modalOkBtnCSS);
				elem.appendChild(mOkBtn);
				mOkBtn.innerHTML = 'Ok';
				(function(_this, id){
					mOkBtn.onclick = 	function(e){
						if(_this.onmokbtn){
							_this.onmokbtn();
						}
						_this.closeModal(id);
					}
				})(_this, id);
			}
			//*************
			if(onmokbtn){
				var mCancelBtn = document.createElement('button');
				this.setCSS(mCancelBtn, modalOkBtnCSS);
				elem.appendChild(mCancelBtn);
				mCancelBtn.innerHTML = '������';
				mCancelBtn.style.marginLeft = '20px';
				(function(_this, id){
					mCancelBtn.onclick = 	function(e){
						//console.log(this);
						_this.closeModal(id);
					}
				})(_this, id);
			}
		},
		setCSS:function(elem, args){
			for(var j in args){
				//console.log(j);
				elem.style[j] = args[j];
			}
		},
		closeModal:function(bgId){
			for(var j=0; j<modalElements.length; j++){
				if(document.getElementById(modalElements[j]+bgId)){
					obj = document.getElementById(modalElements[j]+bgId);
					obj.parentNode.removeChild(obj);
				}
			}
			this.title = false;
			if(this.oncloseafter){
				this.oncloseafter();
			}
			this.onclosebefore = false;
			this.oncloseafter = false;
			this.onmokbtn = false;
			this.showOkBtn = true;
		},
		getTopId:function(){
			return id-1;
		}
	}
})();